# End Node Source Project Repository

The node's firmware is kept as a stm32CubeIDE project within the [end_node](./end_node/) folder. To see the firmware implementation, please load the [end_node](./end_node/) into the stm32CubeIDE app for proper project interpretation. Our implementation is build around the open-source I-CUBE-LRWAN Expansion package from ST Microelectonics.
