/*
 * generator.h
 *
 *  Created on: 15. 7. 2021
 *      Author: Martin Suda
 */

#ifndef APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_GENERATOR_H_
#define APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_GENERATOR_H_


/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stm32l072xx.h"
#include "rxData_parser.h"

/* Private variables ---------------------------------------------------------*/
#define SIN_PROTOTYPE_LEN 128						// length of the prototype array
#define PERIFERAL_CLK 32000000						// 32 MHz -> DAC and PWM timers = APB1, APB2
#define TIM_DAC_MOD TIM6							// timer used for dac in dac.c
#define TIM_PWM_MOD TIM21							// timer used for pwm in pwm.c

static const uint16_t sin_prototype[SIN_PROTOTYPE_LEN] = {2048, 2145, 2242, 2339, 2435, 2530,
		  2624, 2717, 2808, 2897, 2984, 3069, 3151, 3230, 3307, 3381, 3451, 3518, 3581, 3640,
		  3696, 3748, 3795, 3838, 3877, 3911, 3941, 3966, 3986, 4002, 4013, 4019, 4020, 4016,
		  4008, 3995, 3977, 3954, 3926, 3894, 3858, 3817, 3772, 3722, 3669, 3611, 3550, 3485,
		  3416, 3344, 3269, 3191, 3110, 3027, 2941, 2853, 2763, 2671, 2578, 2483, 2387, 2291,
		  2194, 2096, 1999, 1901, 1804, 1708, 1612, 1517, 1424, 1332, 1242, 1154, 1068, 985,
		  904, 826, 751, 679, 610, 545, 484, 426, 373, 323, 278, 237, 201, 169, 141, 118, 100,
		  87, 79, 75, 76, 82, 93, 109, 129, 154, 184, 218, 257, 300, 347, 399, 455, 514, 577,
		  644, 714, 788,865, 944, 1026, 1111, 1198, 1287, 1378, 1471, 1565, 1660, 1756, 1853,
		  1950, 2047 };  										// 12bit sinus value table (SIN prototype)

extern uint16_t sin_output[SIN_PROTOTYPE_LEN];		// declaration of the 12 bit sinus values for specific configuration

/* Private macros -------------------------------------------------------------*/

/* PWM on pin PB14 */
//TODO: set/reset output reg. ODR doesn't need to be there, i think
// reset output reg. & Capture/Comp. output 2 enable & counter enable GPIOB->BSRR |= GPIO_BSRR_BR_14;
#define PWM_START { \
	TIM_PWM_MOD->CCER |= 0x01 << 4U; \
	TIM_PWM_MOD->CR1 |= TIM_CR1_CEN; \
}
// set output reg. & Capture/Comp. output 2 disable & counter disable
#define PWM_STOP { \
	TIM_PWM_MOD->CCER &= ~(TIM_CCER_CC2E); \
	TIM_PWM_MOD->CR1 &= ~(TIM_CR1_CEN); \
}

/* DAC on pin PA4 */

// counter enable
#define DAC_START { \
	TIM_DAC_MOD->CR1 |= TIM_CR1_CEN; \
}
// counter disable
#define DAC_STOP { \
	TIM_DAC_MOD->CR1 &= ~(TIM_CR1_CEN); \
}

/* PWM set if PA9=D8=0 */
#define MUX_SET_PWM { \
	GPIOA->BSRR = GPIO_BSRR_BR_9; \
}

/* DAC set if PA9=D8=1 */
#define MUX_SET_DAC { \
	GPIOA->BSRR = GPIO_BSRR_BS_9; \
}


/* Private functions prototypes -----------------------------------------------*/
void set_light_configuration(LghtConfigData_t light_config_data);
void set_sin_modulation(sin_t sin_config_data);
void set_pwm_modulation(pwm_t pwm_config_data);
void adjust_PSC_ARR(TIM_TypeDef* tim, uint32_t fsig, int Ns);
float absolute_error(float arr, int psc, int Ns, int f_sig, int f_clk);

#endif /* APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_GENERATOR_H_ */
