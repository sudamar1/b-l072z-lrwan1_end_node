/*
 * dac.h
 *
 *  Created on: 15. 7. 2021
 *      Author: Martin Suda
 */

#ifndef APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_DAC_H_
#define APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_DAC_H_


/* Includes ------------------------------------------------------------------*/
#include "stm32l072xx.h"

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
void init_DAC_with_DMA();

#endif /* APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_DAC_H_ */
