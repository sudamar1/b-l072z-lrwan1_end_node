/*
 * mux_switch.h
 *
 *  Created on: 29. 7. 2021
 *      Author: Martin Suda
 */

#ifndef APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_MUX_SWITCH_H_
#define APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_MUX_SWITCH_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32l072xx.h"

/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void init_mux_switch();

#endif /* APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_MUX_SWITCH_H_ */
