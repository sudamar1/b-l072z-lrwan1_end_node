/*
 * hw_init.h
 *
 *  Created on: 25. 8. 2021
 *      Author: Martin Suda
 */

#ifndef APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_HW_INIT_H_
#define APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_HW_INIT_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32l072xx.h"

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
void hw_init_light_generator();

#endif /* APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_HW_INIT_H_ */
