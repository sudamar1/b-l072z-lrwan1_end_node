/*
 * mux_switch.c
 *
 *  Created on: 29. 7. 2021
 *      Author: Martin Suda
 */

/* Includes ------------------------------------------------------------------*/
#include "mux_switch.h"

/* Private functions ---------------------------------------------------------*/
//void init_mux_switch(){		// PB5 = D4
//	/*init GPIO B*/
//	RCC->IOPENR |= RCC_IOPENR_GPIOBEN;			// enable clk to GPIOB
//	GPIOB->MODER &= ~GPIO_MODER_MODE5_Msk;		// reset MODER register
//	GPIOB->MODER |= GPIO_MODER_MODE5_0;			// general purpose output mode
//	GPIOB->OTYPER &= ~GPIO_OTYPER_OT_4;			// push, pull by default
//	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEED4_0;	// medium speed
//	GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPD4);		// no pull-down, pull-up resistor
//	GPIOB->BSRR = GPIO_BSRR_BS_5;				// set PB5=D4 -> DAC as default
//}


void init_mux_switch(){		// PA9 = D8
	/*init GPIO A*/
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN;			// enable clk to GPIOA
	GPIOA->MODER &= ~GPIO_MODER_MODE9_Msk;		// reset MODER register
	GPIOA->MODER |= GPIO_MODER_MODE9_0;			// general purpose output mode
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT_4;			// push, pull by default
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEED4_0;	// medium speed
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPD4);		// no pull-down, pull-up resistor
	GPIOA->BSRR = GPIO_BSRR_BS_9;				// set PA9=D8 -> DAC as default
}
