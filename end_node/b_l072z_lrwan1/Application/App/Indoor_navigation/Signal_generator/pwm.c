/*
 * pwm.c
 *
 *  Created on: 28. 7. 2021
 *      Author: Martin Suda
 */

/* Includes ------------------------------------------------------------------*/
#include "pwm.h"
//#include "stm32l0xx_hal_tim.h"


/* Private functions ---------------------------------------------------------*/
void init_PWM(){  // PWM on PB14=D12 -> TIM21 ch.2
	/*
	 * Initializes PWM mode on Pin D12 through TIM21_CH2
	 *
	 * NOTE: alternative pin to use for PWM is TIM 21 ch. 1 => PB13=D3
	 */

	RCC->APB2ENR |= RCC_APB2ENR_TIM21EN;		// enable clk to TIM21 => APB2 32MHz = f_CK_PSC for PSC=0
	TIM21->CR1 &= ~(TIM_CR1_DIR | TIM_CR1_CMS);	// DIR reset = UP counter CMS = edge aligned mode
	TIM21->CR1 &= ~TIM_CR1_CKD;  				// no clk division
//	TIM21->CR1 &= ~TIM_CR1_ARPE; 				// Set the auto-reload preload disable buffering
	TIM21->CR1 |= TIM_CR1_ARPE; 				// Set the auto-reload preload enable buffering
	TIM21->PSC = 0;								// counter clk freq CK_CNT = f_CK_PSC / (PSC[15:0] + 1)
	TIM21->ARR = 65535;							// min freq. => 32M/65536= 488Hz => 16 bit reg => 2^16 = 65536
	TIM21->CCR2 |= (TIM21->ARR/2);				// 50% duty cycle
	TIM21->EGR |= TIM_EGR_UG;					// reinit CNT, update of regs. ...

	TIM21->SMCR &= ~(TIM_SMCR_SMS | TIM_SMCR_TS | TIM_SMCR_ETF
			| TIM_SMCR_ETPS | TIM_SMCR_ECE | TIM_SMCR_ETP);		// slave mode register (disable external...)
	TIM21->CR2 &= ~TIM_CR2_MMS;			// reset master mode selection
	TIM21->SMCR &= ~TIM_SMCR_MSM;		// master/slave mode = no action

	TIM21->CCER &= ~TIM_CCER_CC2E; 		// output polarity = active high
	TIM21->CCER &= ~TIM_CCER_CC2P; 		// Reset the Output Polarity level
	TIM21->CCER &= ~(TIM_CCER_CC2E); 	// Set the Output Compare Polarity = OC1 is not active

	TIM21->CCMR1 &= ~TIM_CCMR1_CC2S;		// CC2 channel as output
	TIM21->CCMR1 &= ~TIM_CCMR1_OC2M; 		// output mode reset
	TIM21->CCMR1 |= TIM_CCMR1_OC2M_PWM1;	// Select the Output Compare Mode
	TIM21->CCMR1 |= TIM_CCMR1_OC2PE;  		// TIMx_CCR1 preload val. is loaded into reg. at each update event
	TIM21->CCMR1 &= ~TIM_CCMR1_OC2FE; 		// CC2 fast mode disable => compare not reduced to 3 clk cycles

	/* init GPIO B */
	RCC->IOPENR |= RCC_IOPENR_GPIOBEN;					// enable clk to GPIOB port
	GPIOB->MODER &= ~GPIO_MODER_MODE14_Msk;				// reset mode register
	GPIOB->MODER |= (0x02U << GPIO_MODER_MODE14_Pos);	// enable alt. func. mode
	GPIOB->AFR[1] |= (0x06U << GPIO_AFRH_AFSEL14_Pos);	// AF 6 = TIM21 ch.2 => d.s. pg. 54
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPD14;  				// no pull up, no pull down
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEED14_1;			// high speed

	/* bridge the PB12=D9 to analog mode to prevent signal demanging the pin and minimize the consumtion*/
	GPIOB->MODER |= GPIO_MODER_MODE12;					// set analog mode - exclude schmitt...
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPD12_Msk;				// no pull up, no pull down
}

// TODO: Old version - REMOVE!
//void init_PWM(){	// PWM on PB14=D12 -> TIM21 ch.2
//
//	// NOTE: alternative pin to use for PWM is TIM 21 ch. 1 => PB13=D3
//
//	/* init GPIO B */
//	RCC->IOPENR |= RCC_IOPENR_GPIOBEN;						// enable clk to GPIOB port
//	GPIOB->MODER |= (GPIO_MODER_MODE14_1);					// enable alt. func. mode
//	GPIOB->AFR[1] |= (0x6 << GPIO_AFRH_AFSEL14_Pos);		// AF 6 = TIM21 ch.2 => d.s. pg. 54
//
//	/* init TIM 21 */										// ARR -> freq, CCRx -> duty cycle
//	RCC->APB2ENR |= RCC_APB2ENR_TIM21EN;					// enable clk to TIM21 => APB2 32MHz = f_CK_PSC for PSC=0
//	TIM21->CR1 &= ~TIM_CR1_ARPE_Msk;
//
//	TIM21->PSC = 0;											// counter clk freq CK_CNT = f_CK_PSC / (PSC[15:0] + 1)
//	TIM21->ARR = 65535;										// min freq. => 32M/65536= 448Hz => 16 bit reg => 2^16 = 65536
//	TIM21->EGR |= TIM_EGR_UG;								// reinit of CNT, update of regs. ...
//	TIM21->CCR2 |= (TIM21->ARR/2);							// 50% duty cycle
//	TIM21->CCMR1 |= TIM_CCMR1_CC2S | TIM_CCMR1_OC2M_PWM1	// TIM21 as output & function as PWM1 (1 = TIMx_CNT<TIMx_CCR1)
//			| TIM_CCMR1_OC2PE;								// TIMx_CCR1 preload val. is loaded into reg. at each update event
////			| TIM_CCMR1_OC2FE;								// compare 2 fast enable => compare reduced to 3 clk cycles
//
//
//}

//void init_PWM(void)
//{
//
//  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
//  TIM_MasterConfigTypeDef sMasterConfig = {0};
//  TIM_OC_InitTypeDef sConfigOC = {0};
//  TIM_HandleTypeDef htim21;
//
//  htim21.Instance = TIM21;
//  htim21.Init.Prescaler = 0;
//  htim21.Init.CounterMode = TIM_COUNTERMODE_UP;
//  htim21.Init.Period = 65535;
//  htim21.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//  htim21.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
//  if (HAL_TIM_Base_Init(&htim21) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
//  if (HAL_TIM_ConfigClockSource(&htim21, &sClockSourceConfig) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  if (HAL_TIM_PWM_Init(&htim21) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
//  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
//  if (HAL_TIMEx_MasterConfigSynchronization(&htim21, &sMasterConfig) != HAL_OK)
//  {
//    Error_Handler();
//  }
//  sConfigOC.OCMode = TIM_OCMODE_PWM1;
//  sConfigOC.Pulse = htim21.Init.Period/2;
//  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
//  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
//  if (HAL_TIM_PWM_ConfigChannel(&htim21, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
//  {
//    Error_Handler();
//  }
//
//  HAL_TIM_MspPostInit(&htim21);
//
//}
