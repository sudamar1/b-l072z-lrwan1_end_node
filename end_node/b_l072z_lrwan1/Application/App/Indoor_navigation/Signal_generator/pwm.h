/*
 * pwm.h
 *
 *  Created on: 28. 7. 2021
 *      Author: Martin Suda
 */

#ifndef APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_PWM_H_
#define APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_PWM_H_


/* Includes ------------------------------------------------------------------*/
#include "stm32l072xx.h"

/* Private variables ---------------------------------------------------------*/
#define TIM_CCMR1_OC2M_PWM1 (0x06 << TIM_CCMR1_OC2M_Pos)		// sets PWM1 mode to TIM21 ch.2 in CCMR1 reg.

/* Private functions ---------------------------------------------------------*/
void init_PWM();

#endif /* APPLICATION_APP_INDOOR_NAVIGATION_SIGNAL_GENERATOR_PWM_H_ */
