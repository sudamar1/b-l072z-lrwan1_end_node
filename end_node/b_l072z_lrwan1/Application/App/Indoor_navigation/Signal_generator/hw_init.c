/*
 * hw_init.c
 *
 *  Created on: 25. 8. 2021
 *      Author: Martin Suda
 */


/* Includes ------------------------------------------------------------------*/
#include "dac.h"
#include "pwm.h"
#include "mux_switch.h"
#include "sys_app.h"

/* Private functions ----------------------------------------------------------*/
void hw_init_light_generator(){
	/*Initial initialization of hardware for light modulation*/
	APP_LOG(TS_OFF, VLEVEL_M, "HW INIT: Starts...\r\n")
	init_DAC_with_DMA();
	init_PWM();
	init_mux_switch();
	APP_LOG(TS_OFF, VLEVEL_M, "HW INIT: Done\r\n")
}
