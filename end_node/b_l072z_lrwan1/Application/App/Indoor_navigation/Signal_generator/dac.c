/*
 * dac.c
 *
 *  Created on: 15. 7. 2021
 *      Author: Martin Suda
 */


/* Includes ------------------------------------------------------------------*/
#include "dac.h"
#include "generator.h"

/* Private variables ---------------------------------------------------------*/
uint16_t sin_output[SIN_PROTOTYPE_LEN] = {0};  // define extern variable

/* Private functions ----------------------------------------------------------*/
void init_DAC_with_DMA(void){ 				// CPU running on 32MHz periferals also APB1

	/* Init TIM6 as DAC trigger */
	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN; 		// enable clk to TIM6
	TIM6->PSC = 32-1;							// prescaler => PSC*1/f = 32*1/32MHz = 1us
	TIM6->ARR = 10-1;							// reload in 10 us => 10 kHz sampling
	TIM6->CR1 &= ~TIM_CR1_ARPE;					// auto reload preload disable
	TIM6->CR2 |= TIM_CR2_MMS_1;  				// update - enable TRGO signal - trigger for DMA

	/* DAC output as analog (pin PA4=A2) */
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN;			// enable clk to GPIOA
	GPIOA->MODER &= ~GPIO_MODER_MODE4;			// reset moder register
	GPIOA->MODER |= GPIO_MODER_MODE4;  			// set analog mode on GPIOA 4 (0x3UL)

	/* Init DAC */
	RCC->APB1ENR |= RCC_APB1ENR_DACEN; 				// enable clk to DAC
	DAC->CR |= DAC_CR_TEN1;							// DAC trigger enable
	DAC->CR &= ~DAC_CR_TSEL1;						// DAC trigger set to TIM6 => (0x0UL)
	DAC->CR &= ~DAC_CR_BOFF1;						// output buffer enable
	DAC->CR &= ~DAC_CR_WAVE1;						// DAC ch. 1 wave gen. disable
//	DAC->CR |= DAC_CR_MAMP1;						// max amplitude mask
	DAC->CR |= (DAC_CR_DMAUDRIE1 | DAC_CR_DMAEN1);	// DMA underrun enable & DAC ch.1 DMA enable

	// TODO: DMA underrun fallback implementation => ref.m. pg. 360
//	uint16_t sin_output[SIN_PROTOTYPE_LEN] = {0};	// clear output buffer
	for (int i=0; i<SIN_PROTOTYPE_LEN; i++){  		// prepare sin signal to output
		sin_output[i] = sin_prototype[i];
	}

	/* Init DMA */
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;						// enable clk to DMA 1
	DMA1_CSELR->CSELR |= (uint32_t) (9 << 4);				// map DAC on DMA ch. 2 => ref.m. pg.264, Table 51.
	DMA1_Channel2->CPAR |= (uint32_t) (&DAC->DHR12R1);		// configure the peripheral data reg. address => from DMA here
//	DMA1_Channel2->CMAR |= (uint32_t) (&sin_prototype);		// configure the memory address => sig data address
	DMA1_Channel2->CMAR |= (uint32_t) (&sin_output);		// configure the memory address => sig data address
	DMA1_Channel2->CNDTR = SIN_PROTOTYPE_LEN;				// number of DMA transfers to be performed on DMA ch. 2
	DMA1_Channel2->CCR &= ~(DMA_CCR_PINC | DMA_CCR_PL);
	DMA1_Channel2->CCR &= ~(DMA_CCR_MSIZE | DMA_CCR_PSIZE);
	DMA1_Channel2->CCR |= DMA_CCR_MINC;
	DMA1_Channel2->CCR |=  DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0    // memory size 16 bits & peripheral size 16 bits
							| DMA_CCR_DIR  						// direction - read from mem.
							| DMA_CCR_CIRC;						// circular mode

	/* NVIC config for DMA */
//	NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0);	// set high priority on DMA ch. 2 interrupt
//	NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);		// enable interrupt on DMA ch. 2

	DMA1_Channel2->CCR |= DMA_CCR_EN;				// enable DMA ch. 2
	DAC->CR |= DAC_CR_EN1;							// enable DAC ch. 1
}

/*OLD VERSION*/
//void init_DAC_with_DMA(){ 				// CPU running on 32MHz periferals also APB1
//	/* Init TIM6 as DAC trigger */
//	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN; 		// enable clk to TIM6
//	TIM6->PSC = 32-1;							// prescaler => PSC*1/f = 32*1/32MHz = 1us
//	TIM6->ARR = 10-1;							// reload in 10 us => 10 kHz sampling
//	TIM6->CR2 |= TIM_CR2_MMS_1;  				// update - enable TRGO signal - trigger for DMA
//	TIM6->CR1 &= ~TIM_CR1_ARPE;						// auto reload preload disable
//	/* DAC output as analog (pin PA4=A2) */
//	RCC->IOPENR |= RCC_IOPENR_GPIOAEN;			// enable clk to GPIOA
//	GPIOA->MODER &= ~GPIO_MODER_MODE4;			// reset moder register
//	GPIOA->MODER |= GPIO_MODER_MODE4;  			// set analog mode on GPIOA 4 (0x3UL)
//
//	/* Init DAC */
//	RCC->APB1ENR |= RCC_APB1ENR_DACEN; 				// enable clk to DAC
//	DAC->CR |= DAC_CR_TEN1;							// DAC trigger enable
//	DAC->CR &= ~DAC_CR_TSEL1;						// DAC trigger set to TIM6 => (0x0UL)
//	DAC->CR &= ~DAC_CR_BOFF1;						// output buffer enable
//	DAC->CR &= ~DAC_CR_WAVE1;						// DAC ch. 1 wave gen. disable
//
////	DAC->CR |= DAC_CR_MAMP1;						// max amplitude mask
//	DAC->CR |= (DAC_CR_DMAUDRIE1 | DAC_CR_DMAEN1);	// DMA underrun enable & DAC ch.1 DMA enable
//	DAC->CR |= DAC_CR_EN1;							// enable DAC ch. 1
//
//	// TODO: DMA underrun fallback implementation => ref.m. pg. 360
//	uint16_t sin_output[SIN_PROTOTYPE_LEN] = {0};	// clear output buffer
//	for (int i=0; i<SIN_PROTOTYPE_LEN; i++){  		// prepare sin signal to output
//		sin_output[i] = sin_prototype[i];
//	}
//
//	/* Init DMA */
//	RCC->AHBENR |= RCC_AHBENR_DMA1EN;							// enable clk to DMA 1
//	DMA1_CSELR->CSELR |= (uint32_t) (9 << 4);					// map DAC on DMA ch. 2 => ref.m. pg.264, Table 51.
//	DMA1_Channel2->CPAR |= (uint32_t) (&(DAC->DHR12R1));		// configure the peripheral data reg. address => from DMA here
//	DMA1_Channel2->CMAR |= (uint32_t) sin_output;				// configure the memory address => sig data address
//	DMA1_Channel2->CNDTR |= SIN_PROTOTYPE_LEN;					// number of DMA transfers to be performed on DMA ch. 2
//	DMA1_Channel2->CCR |=  DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0    // memory size 16 bits & peripheral size 16 bits
//							| DMA_CCR_DIR | DMA_CCR_TEIE  		// direction - read from mem. & transf. err. interrupt enabled
//							| DMA_CCR_CIRC;						// circular mode
//	DMA1_Channel2->CCR |= DMA_CCR_EN;							// enable DMA ch. 2
//
//	/* NVIC config for DMA */
////	NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);		// enable interrupt on DMA ch. 2
////	NVIC_SetPriority(DMA1_Channel2_3_IRQn, 3);	// set high priority on DMA ch. 2 interrupt
//}
