/*
 * generator.c
 *
 *  Created on: 15. 7. 2021
 *      Author: Martin Suda
 */
/* Includes ------------------------------------------------------------------*/
#include "generator.h"
#include "sys_app.h"
#include <stdio.h>
#include <stdlib.h>

/* Private variables ---------------------------------------------------------*/
//uint16_t sin_output[SIN_PROTOTYPE_LEN] = {0};

/* Private functions ---------------------------------------------------------*/
void set_light_configuration(LghtConfigData_t light_config_data){
	APP_LOG(TS_ON, VLEVEL_M, "LIGHT CONF: Setting light modulation\r\n");
	char * out = malloc(100*sizeof(char)); //TODO: create a log function, so u dont handle mem alloc here

	switch (light_config_data.modulation)
	{
		case 1:	// pwm
			APP_LOG(TS_ON, VLEVEL_M, "PWM: Setting ...\r\n");
			sprintf(out, "PWM: Parametrs:\r\nFrequency:\t%lu Hz,\r\nDuty Cycle:\t%u %%\r\n",
					light_config_data.pwmParams.frequency, light_config_data.pwmParams.dutyCycle);
			APP_LOG(TS_ON, VLEVEL_H, out);
			set_pwm_modulation(light_config_data.pwmParams);
			APP_LOG(TS_ON, VLEVEL_M, "PWM: Configuration done.\r\n");
			break;

		case 2:	// sin
			APP_LOG(TS_ON, VLEVEL_M, "SIN: Setting ...\r\n");
			sprintf(out, "SIN: Parametrs:\r\nFrequency:\t%lu Hz,\r\nAmplitude:\t%u %%,\r\nPhase:\t%u deg\r\n",
					light_config_data.sinParams.frequency, light_config_data.sinParams.amplitude, light_config_data.sinParams.phase);
			APP_LOG(TS_ON, VLEVEL_H, out);
			set_sin_modulation(light_config_data.sinParams);
			break;

		default: // else
			APP_LOG(TS_ON, VLEVEL_L, "LIGHT CONF: Default switch choice\r\n");

	}
	free(out);
}

void set_sin_modulation(sin_t sin_config_data){
	DAC_STOP;
	// adjust PSC & ARR register
	adjust_PSC_ARR(TIM_DAC_MOD, sin_config_data.frequency, SIN_PROTOTYPE_LEN);
	// adjust amplitude of sin prototype
	for (int i = 0; i < SIN_PROTOTYPE_LEN; i++){
		sin_output[i] =
				(uint16_t)( (float)(sin_config_data.amplitude/100.0) * (float) sin_prototype[i] );
	}
	// set phase by shifting counter
	TIM_DAC_MOD->CNT = (uint32_t)( (float)TIM_DAC_MOD->ARR *
			((float) sin_config_data.phase/360.0) );
	// dedug
	char * out = malloc(80*sizeof(char));
	sprintf(out, "sin out amp check (3 elmt): 1: %u, 2: %u, 3: %u, cnt: %lu\r\n", sin_output[0], sin_output[1], sin_output[2], TIM_DAC_MOD->CNT);
	APP_LOG(TS_OFF, VLEVEL_L, out);
	free(out);
}

void set_pwm_modulation(pwm_t pwm_config_data){
	PWM_STOP;
	// TODO: debug this function PROBLEM -> no pwm generated
	adjust_PSC_ARR(TIM_PWM_MOD, pwm_config_data.frequency, 1);

	uint32_t tmp = (uint32_t) ( TIM_PWM_MOD->ARR *
				((float) pwm_config_data.dutyCycle/100.0)); //TODO: if works good every time?
	TIM_PWM_MOD->CCR2 = tmp;			// set duty cycle
	TIM_PWM_MOD->EGR |= TIM_EGR_UG;
	// dedug
	char * out = malloc(60*sizeof(char));
	sprintf(out, "duty: %lu, arr: %lu, psc: %lu\r\n", tmp, TIM_PWM_MOD->ARR, TIM_PWM_MOD->PSC);
	APP_LOG(TS_OFF, VLEVEL_L, out);
	sprintf(out, "ccr1: %lu\r\n", TIM_PWM_MOD->CCR1);
	APP_LOG(TS_OFF, VLEVEL_L, out);
	// TODO: include phase
	free(out);
}

/*https://stackoverflow.com/questions/51906691/stm32f4-timers-calculation-of-period-and-prescale-and-generating-1-ms-delay*/
// TODO: investigate precision of the adjustment => might have multiple solutions some better precision
void adjust_PSC_ARR(TIM_TypeDef* tim, uint32_t fsig, int Ns){
	/* Adjust the PSC and ARR register in order to set wanted modulation frequency
	 *
	 * NOTE:
	 * https://deepbluembedded.com/stm32-dac-sine-wave-generation-stm32-dac-dma-timer-example/
	 * https://github.com/v0idv0id/STM32-Scaler
	 * SIN
	 * => f_trig = F_clk/((PSC+1)(ARR+1))
	 * => f_sig = f_trig/Ns
	 * PWM
	 * => Update_freq = TIM clk / ((PSC+1)*(ARR+1)*(RCR(=0)+1))
	 * => Update_freq = fsig => PSC = ((TIM_clk/update_freq)-1)/(ARR+1)
	 * */
	int p = PERIFERAL_CLK/fsig;
	int arr;
//	float abs_err;
//	float threshold = 0.01;  // 1% error enabled

	for ( int psc = 0; psc < 65536; psc++ ){
		arr = (p/(Ns*(psc+1)))-1; // or arr = (p-psc)/(psc+1); alternative
//		abs_err = absolute_error(arr, psc, Ns, fsig, PERIFERAL_CLK);
//		if ( ((int) arr >= 0) && ((int) arr < 65536) && (abs_err < threshold)){
		if ( (arr > 0) && (arr < 65536) ){
			tim->ARR = (uint32_t) arr;
			tim->PSC = (uint32_t) psc;
			// dedug
			char * out = malloc(80*sizeof(char));
			sprintf(out, "ARR: %lu, PSC: %lu\r\n", tim->ARR, tim->PSC);
			APP_LOG(TS_OFF, VLEVEL_L, out);
			free(out);
			break;
		}
	}

}

float absolute_error(float arr, int psc, int Ns, int f_sig, int f_clk){
	float f_sig_counted = (f_clk/((psc+1)*(arr+1)))/Ns;
	int f_sig100 = f_sig*10000;
	return (float) abs((int) (f_sig_counted*10000) - f_sig100)/f_sig100;
}
