/*
 * rxData_parser.h
 *
 *  Created on: 15. 7. 2021
 *      Author: Martin Suda
 */

#ifndef APPLICATION_APP_INDOOR_NAVIGATION_LORA_MESSAGE_RXDATA_PARSER_H_
#define APPLICATION_APP_INDOOR_NAVIGATION_LORA_MESSAGE_RXDATA_PARSER_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Private typedef -----------------------------------------------------------*/
typedef struct
{
	uint32_t frequency;
	uint8_t dutyCycle;

} pwm_t;

typedef struct
{
	uint32_t frequency;
	uint8_t amplitude;
	uint16_t phase;

} sin_t;

typedef struct
{
	uint8_t modulation;
	pwm_t pwmParams;
	sin_t sinParams;
} LghtConfigData_t;

/* Private function prototypes -----------------------------------------------*/
void parse_rxData(uint8_t* rxData, uint8_t rxDataSize, LghtConfigData_t* light_config_data);
void uint_as_char(uint32_t num, char * out);
void log_raw_rxData(uint8_t* data);

#endif /* APPLICATION_APP_INDOOR_NAVIGATION_LORA_MESSAGE_RXDATA_PARSER_H_ */
