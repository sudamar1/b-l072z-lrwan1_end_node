/*
 * rxData_parser.c
 *
 *  Created on: 15. 7. 2021
 *      Author: Martin Suda
 */

/* Includes ------------------------------------------------------------------*/
#include "rxData_parser.h"
#include "sys_app.h"
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

/* Exported functions ---------------------------------------------------------*/
void parse_rxData(uint8_t* rxData, uint8_t rxDataSize, LghtConfigData_t* light_config_data){
	/*
	 * Parses the LoraWAN payload to a structure of the configuration data
	 * */
	light_config_data->modulation = rxData[0];  // get modulation type

	char* out;
	// TODO: refactor log purposes so no alloc neccesary here!
	switch(light_config_data->modulation)
	{
		case 1: ;  // pwm
			light_config_data->pwmParams.frequency = (rxData[1] << 24) | (rxData[2] << 16) | (rxData[3] << 8) | rxData[4];
			light_config_data->pwmParams.dutyCycle = rxData[5];
			//log purposes
			out = malloc(100*sizeof(char));
			sprintf(out, "LGHT PARSER: Received configuration:\r\nModulation:\tPWM(%d),\r\nFrequency:\t%lu Hz,\r\nDutyCycle:\t%u\r\n", light_config_data->modulation,
					(unsigned long)(light_config_data->pwmParams.frequency), light_config_data->pwmParams.dutyCycle);
			APP_LOG(TS_OFF, VLEVEL_H, out);
			free(out);
			break;

		case 2: ;  // sinus
			light_config_data->sinParams.frequency = (rxData[1] << 24) | (rxData[2] << 16) | (rxData[3] << 8) | rxData[4];
			light_config_data->sinParams.amplitude = rxData[5];
			light_config_data->sinParams.phase = (rxData[6] << 8) | rxData[7];
			// log purposes
			out = malloc(120*sizeof(char));
			sprintf(out, "LGHT PARSER: Received configuration:\r\nModulation:\tSINUS,\r\nFrequency:\t%lu Hz,\r\nAmplitude:\t%u %%,\r\nPhase:\t%u deg\r\n", (unsigned long) light_config_data->sinParams.frequency,
					light_config_data->sinParams.amplitude, light_config_data->sinParams.phase);
			APP_LOG(TS_OFF, VLEVEL_L, out);
			free(out);
			break;
		default:
			APP_LOG(TS_ON, VLEVEL_M, "MODULATION UNKNOWN!\r\n");
	}
}

void save_pwm_configuration(){

}

void uint_as_char(uint32_t num, char * out){
	out = malloc(10*sizeof(char));
	memset(out, 0, 10*sizeof(char));
	sprintf(out, "%ld\n", num);
}

void log_raw_rxData(uint8_t* rxData){
	APP_LOG(TS_ON, VLEVEL_M, "RxDATA:\r\n");
	uint32_t freq = (rxData[0] << 24) | (rxData[1] << 16) | (rxData[2] << 8) | rxData[3];
	char * s = malloc(10*sizeof(char));
	memset(s, 0, 10*sizeof(char));
	sprintf(s, "%ld\n", freq);
	APP_LOG(TS_OFF, VLEVEL_M, s);
//	APP_LOG(TS_OFF, VLEVEL_L, (char *) rxData);
	APP_LOG(TS_OFF, VLEVEL_M, "\r\nEND DATA\r\n");
}

void log_config_data(){

}
