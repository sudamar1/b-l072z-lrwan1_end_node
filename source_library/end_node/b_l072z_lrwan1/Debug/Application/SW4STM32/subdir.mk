################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
C:/Users/admin/Desktop/b-l072z-lrwan1_end_node/stm32CubeIDE_project/end_node/startup_stm32l072xx.s 

C_SRCS += \
C:/Users/admin/Desktop/b-l072z-lrwan1_end_node/stm32CubeIDE_project/end_node/syscalls.c 

OBJS += \
./Application/SW4STM32/startup_stm32l072xx.o \
./Application/SW4STM32/syscalls.o 

C_DEPS += \
./Application/SW4STM32/syscalls.d 


# Each subdirectory must supply rules for building sources it contributes
Application/SW4STM32/startup_stm32l072xx.o: C:/Users/admin/Desktop/b-l072z-lrwan1_end_node/stm32CubeIDE_project/end_node/startup_stm32l072xx.s
	arm-none-eabi-gcc -mcpu=cortex-m0plus -g3 -c -x assembler-with-cpp --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@" "$<"
Application/SW4STM32/syscalls.o: C:/Users/admin/Desktop/b-l072z-lrwan1_end_node/stm32CubeIDE_project/end_node/syscalls.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DCMWX1ZZABZ0XX -c -I../../project_libraries/LoRaWAN/App -I../../project_libraries/Core/Inc -I../../project_libraries/LoRaWAN/Target -I../../core_libraries/Utilities/misc -I../../core_libraries/Utilities/timer -I../../core_libraries/Drivers/BSP/CMWX1ZZABZ_0xx -I../../core_libraries/Drivers/BSP/B-L072Z-LRWAN1 -I../../core_libraries/Utilities/sequencer -I../../core_libraries/Utilities/lpm/tiny_lpm -I../../core_libraries/Utilities/trace/adv_trace -I../../core_libraries/Drivers/STM32L0xx_HAL_Driver/Inc -I../../core_libraries/Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../../core_libraries/Drivers/CMSIS/Include -I../../core_libraries/Middlewares/Third_Party/SubGHz_Phy -I../../core_libraries/Middlewares/Third_Party/SubGHz_Phy/sx1276 -I../../core_libraries/Middlewares/Third_Party/LoRaWAN/Crypto -I../../core_libraries/Middlewares/Third_Party/LoRaWAN/Mac -I../../core_libraries/Middlewares/Third_Party/LoRaWAN/Mac/Region -I../../core_libraries/Middlewares/Third_Party/LoRaWAN/Utilities -I../../core_libraries/Middlewares/Third_Party/LoRaWAN/LmHandler -I../../core_libraries/Middlewares/Third_Party/LoRaWAN/LmHandler/packages -I../../core_libraries/Drivers/BSP/IKS01A2 -I../../core_libraries/Drivers/BSP/Components/Common -I../../core_libraries/Drivers/BSP/Components/hts221 -I../../core_libraries/Drivers/BSP/Components/lps22hb -I../../core_libraries/Drivers/BSP/Components/lsm6dsl -I../../core_libraries/Drivers/BSP/Components/lsm303agr -Os -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Application/SW4STM32/syscalls.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

