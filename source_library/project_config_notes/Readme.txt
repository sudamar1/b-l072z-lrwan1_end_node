Reconfigure project
- paths need to be reconfigured in all *.d files within Dedug folder - Create Python script
- .project file needs to be edited
	- parent paths should be changed (or through IDE e.g. picture parent_paths_from_project_file_in_ide.png) and individual paths to *.c files
	- name can be changed within this file
- .cproject - holds include paths, but can be edited via IDE e.g. picture include_paths_in_ide.png